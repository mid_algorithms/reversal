import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Scanner;
public class Reversal {
	
	public static void main(String[] args) {
		
		try {
			
			// Count File input and output 10 time
			for(int n=1;n<=10;n++) {
				
				// Input File .in
				File file = new File(n+".in");
	        	InputStream inputStream = new FileInputStream(file);
	        	
	            Scanner scanner = new Scanner(inputStream);
	            String data =  scanner.nextLine();         
	            data = data.substring(1,data.length()-1);       
	            String[] A_String = data.split(",");   
	            int[] A = new int[A_String.length];
	            for(int i = 0; i < A_String.length; i++){
	                A[i] = Integer. parseInt(A_String[i]);
	            }

	            // Output
	            arrayReverse(A);
	    		System.out.println("output = "+ Arrays.toString(A));
	            
	    		// File Output .out
	    		PrintWriter writer;
	    		try {
	    			writer = new PrintWriter(n+".out", "UTF-8");
	    			writer.print(Arrays.toString(A));
	    			writer.close();
	    		} catch (FileNotFoundException e) {
	    			e.printStackTrace();
	    		} catch (UnsupportedEncodingException e) {
	    			e.printStackTrace();
	    		}
			} 
        } catch (Exception e) {
            e.printStackTrace ();
        }

	}
	
	// Algorithm
	static int arrayReverse(int[] A) {
		int start = 0;
		int end = A.length-1;
		while(start<end){
			int temp = A[start];
			A[start] = A[end];
			A[end] = temp;
			
			start = start + 1;
			end = end - 1;
			
		}
		return start;
	}
}
